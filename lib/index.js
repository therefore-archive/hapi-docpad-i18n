var Hapi = require('hapi'),
    Locale = require('locale');

// Declare internals
var internals = {};

// Defaults
internals.defaults = {};

exports.register = function (plugin, options, next) {

    plugin.hapi.utils.assert(typeof plugin.route === 'function', 'Plugin permissions must allow route');

    var path = (options.path !== undefined) ? options.path : '/api/docpad/setLang',
        supported = new Locale.Locales((options.locales !== undefined) ? options.locales : 'en, fr, es');

    var cookieOptions = {
        ttl: 30 * 24 * 60 * 60 * 1000, // Thirty days till expiry.
        isSecure: false,
        path: '/',
        encoding: 'none'
    };

    var handler = function (request, reply) {
        var langCookie = request.state.lang,
            locales,
            redirect;

        // If there's no language cookie set, get the best match based on the accept-language header.
        if (!langCookie) {

            locales = new Locale.Locales(request.headers['accept-language']);
            langCookie = locales.best(supported).toString();


            Hapi.state.prepareValue('lang', langCookie, cookieOptions, function (err, value) {
                if (err) {
                    return reply(err);
                }

                reply.state('lang', value, cookieOptions );
            });
        }

        // If the language is other than the default (en), set the language cookie and redirect to the homepage
        redirect = (langCookie === 'en') ? false : '/' + langCookie + '/';

        return reply({ redirect : redirect });
    };

    plugin.route({
        method: 'POST',
        path: path,
        handler: handler
    });

    next();
};
